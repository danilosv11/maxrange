package net.threegon;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ThreegonRooterTest {
    @Test(expected = RuntimeException.class)
    public void testDLowerThanZeroOrAIsZero() {
        Threegon tg = new Threegon(0, 1, 1);
        ThreegonRooter tr = new ThreegonRooter(tg);
        tr.getMaxRoot();
    }

    @Test
    public void testDEqualsZero() {
        Threegon tg = new Threegon(1, 2, 1);
        ThreegonRooter tr = new ThreegonRooter(tg);
        assertEquals(tr.getMaxRoot(), -1, 10E-9);
    }

    @Test
    public void testDBiggerThanZero() {
        Threegon tg = new Threegon(1, 2, -3);
        ThreegonRooter tr = new ThreegonRooter(tg);
        assertEquals(tr.getMaxRoot(), 1, 10E-9);
    }

}
