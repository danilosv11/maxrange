package net.threegon;

public class ThreegonRooter {
    private Threegon tg;

    public ThreegonRooter(Threegon tg) {
        this.tg = tg;
    }

    public double getMaxRoot() {
        if (tg.getRoots() == null) {
            throw new RuntimeException();
        } else {
            return (tg.getRoots()[0] > tg.getRoots()[1]) ? tg.getRoots()[0] : tg.getRoots()[1];
        }
    }
}
